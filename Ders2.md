# Ders 2

 [![Teknik Resim][t1]][t2]

[t1]:  Ders2/teknik_resim_res.png
[t2]:  Ders2/teknik_resim.png "Teknik Resim"

Siyah elips ile gösterilen kısımdan “Part Design” sekmesi seçilir. Seçilen sekmede, resimde kırmızı çember ile gösterilen “Create a New Sketch” butonuna tıklanılır. Açılan pencerede çizime başlanılacak düzlem seçildikten sonra, 2 boyutlu çizime başlanılır. 

 [![Şekil 1][1]][2]

[1]:  Ders2/1_res.png
[2]:  Ders2/1.png "Şekil 1"


Tasarımın belirlenen görünümden 2 boyutlu çizimine başlamak için, düzlem seçiminden sonra çıkan çizim ekranında, kırmızı çember ile gösterilen “Create a Line in the Sketch” butonuna tıklanılarak çizgiler çizilir. Çizilen bu çizgilerin, siyah dörtgen ile gösterilen “Fix the Vertical Distance Between Two Points or Line Ends” ve Fix the Horizontal Distance Between Two Points or Line Ends” komutları kullanılarak ölçülendirmeleri yapılır.

 [![Şekil 2][3]][4]

[3]:  Ders2/2_res.png
[4]:  Ders2/2.png "Şekil 2"

Düzlem seçiminden sonra açılan çizim uzayında, kırmızı daire ile gösterilen "Create a Line in the Sketch" komutuna tıklanılarak, çizim yapmaya başlanır. Bu komut ile çizilen yatay çizgiler, siyah daire ile gösterilen "Fix the Horizontal Distance Between Two Point or Line Ends" komutu ile ölçülendirilir. Çizilen dikey çizgiler ise, yatay ölçülendirme komutunun yan kısmında bulunan ve mavi daire ile gösterilen "Fix the Vertical Distance Between Two Point or Line Ends" komutu ile ölçülendirilir.

 [![Şekil 3][5]][6]

[5]:  Ders2/3_res.png
[6]:  Ders2/3.png "Şekil 3"

Taslak çizim tamamlandıktan sonra, resimde kırmızı çember ile gösterilen “Create a Circle in the Sketcher” butonuna tıklanaraki tasarımın ortasındaki delik çizilir. Daha sonra siyah kare ile gösterilen “Constrain an Arc or a Circle” butonuna tıklanarak çizilen deliğin ölçülendirmesi yapılır. Ölçülendirme tamamlandıktan sonra, kırmızı dörtgen içinde gösterilen dikey ve yatay ölçülendirme komutları kullanılarak çemberin çizimdeki pozisyonu tanımlanır.

 [![Şekil 4][7]][8]

[7]:  Ders2/4_res.png
[8]:  Ders2/4.png "Şekil 4"

“Close” butonu ile 2 boyutlu çizim tamamlandıktan sonra, açılan pencerede kırmızı dörtgen ile gösterilen “Pad” butonuna tıklanarak, yapılan 2 boyutlu çizime hacim kazandırma işlemine başlanır.

 [![Şekil 5][9]][10]

[9]:  Ders2/5_res.png
[10]: Ders2/5.png "Şekil 5"

“Pad” butonuna tıklandıktan sonra açılan pencerede “Length” değeri 60 mm girilerek, yapılmış olan 2 boyutlu çizime 60mm lik hacim kazandırılır ve çizim 3 boyutlu hale getirilir

 [![Şekil 6][11]][12]

[11]:  Ders2/6_res.png
[12]:  Ders2/6.png "Şekil 6"

Çizime hacim kazandırıldıktan sonra, oluşturulan yeni gövdenin yan yüzeyi seçilir. Çıkan pencerede, resimde kırmızı elips ile gösterilen “Create a New Sketch” komutuna tıklanır ve yüzey üzerinde çizim yapma işlemine geçilir.

 [![Şekil 7][13]][14]

[13]:  Ders2/7_res.png
[14]:  Ders2/7.png "Şekil 7"

Açılan çizim ekranında, resimde kırmızı çember içerisinde gösterilen “Create a line in the Sketch” butonuna tıklanılarak, kesim işlemi için kullanılacak dörtgenler çizilir ve resimde siyah dörtgen içerisinde gösterilmiş olan dikey ve yatay ölçülendirme komutları kullanılarak çizilen bu dörtgenlerin ölçülendirmeleri ve pozisyonları tanımlanır.

 [![Şekil 8][15]][16]

[15]:  Ders2/8_res.png
[16]:  Ders2/8.png "Şekil 8"

Kesilecek olan dörtgenlerin çizimleri tamamlandıktan sonra, çıkan pencerede resimde kırmızı elips ile gösterilen “Pocket” komutuna tıklanarak, dörtgenlerin kesim işlemine geçilir.

 [![Şekil 9][17]][18]

[17]:  Ders2/9_res.png
[18]:  Ders2/9.png "Şekil 9"

Pocket” komutuna tıklanıldıktan sonra çıkan pencerede “Lenght” değeri 46 mm girilerek kesim işlemi tamamlanır.

 [![Şekil 10][19]][20]

[19]:  Ders2/10_res.png
[20]:  Ders2/10.png "Şekil 10"

Kesim işlemi tamamlandıktan sonra çizimin yan yüzeyine tıklanır ve resimde kırmızı elips ile gösterilen “Create Sketch” butonuna tıklanarak seçilen yüzey üzerinde çizim yapma işlemine başlanır.

 [![Şekil 11][21]][22]

[21]:  Ders2/11_res.png
[22]:  Ders2/11.png "Şekil 11"

Yüzeyde yapılacak çizim için, kırmızı daire ile gösterilen “Create a line in the sketch” komutu ile dörtgen çizilir ve siyah dörtgen içerisinde gösterilen dikey ve yatay ölçülendirme komutları ile çizilen dörtgenin ölçüleri ve pozisyonu belirlenir. 

 [![Şekil 12][23]][24]

[23]:  Ders2/12_res.png
[24]:  Ders2/12.png "Şekil 12"

Dörtgen çizimi tamamlandıktan sonra açılan pencerede “Pocket” komutuna tıklanarak kesim işlemine başlanır.

 [![Şekil 13][25]][26]

[25]:  Ders2/13_res.png
[26]:  Ders2/13.png "Şekil 13"

“Pocket” butonuna tıklandıktan sonra açılan pencerede “Lenght” değeri 46 mm girilir ve kesme işlemi tamamlanır.



