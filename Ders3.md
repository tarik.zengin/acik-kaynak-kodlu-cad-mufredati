# Ders 3

 [![Teknik Resim][t1]][t2]

[t1]:  Ders3/teknik_resim_res.png
[t2]:  Ders3/teknik_resim.png "Teknik Resim"

Çizime başlamak için öncelikle kırmızı elips ile gösterilen sekmede “Part Design” seçeneği seçilir, daha sonra açılan pencerede siyah çember içerisinde gösterilen “Create a New Sketch” butonuna tıklanır.

 [![Şekil 1][1]][2]

[1]:  Ders3/1_res.png
[2]:  Ders3/1.png "Şekil 1"


Açılan pencerede çizimin yapılacağı düzlem seçilir ve çizime başlanır.

 [![Şekil 2][3]][4]

[3]:  Ders3/2_res.png
[4]:  Ders3/2.png "Şekil 2"

Açılan çizim ekranında, kırmızı çember ile gösterilen “Create a Line in the Sketch” komutu ve siyah çember ile gösterilen “Create a Circle in the Sketch” komutu kullanılarak çizim tamamlanır. Kırmızı dörtgen ile gösterilen dikey ve yatay ölçülendirme komutları ve siyah dörtgen ile gösterilen çember ölçülendirme komutu ile çizilen çizgilerin ve çemberin ölçülendirmeleri ve pozisyonları ayarlanır.

 [![Şekil 3][5]][6]

[5]:  Ders3/3_res.png
[6]:  Ders3/3.png "Şekil 3"

Tasarımda bulunan delik kısmı için, kırmızı kare ile gösterilen "Create a Circle in the Sketcher" komutu ile bir çember çizilir. Çizilen bu çember, resimde kırmızı çember içerisinde gösterilmiş olan "Constrain an Arc or a Circle" komutu ile ölçülendirilir. Çemberin çizimi ve ölçülendirmesi tamamlandıktan sonra çember, siyah elips içinde gösterilen dikey ve yatay ölçülendirme komutları kullanılarak çizimde olması gereken  yere taşınır.

 [![Şekil 4][7]][8]

[7]:  Ders3/4_res.png
[8]:  Ders3/4.png "Şekil 4"

2 boyutlu çizim tamamlandıktan sonra, yapılan çizime hacim kazandırmak için, kırmızı elips ile gösterilen “Pad” butonuna tıklanır.

 [![Şekil 5][9]][10]

[9]:  Ders3/5_res.png
[10]: Ders3/5.png "Şekil 5"

“Pad” butonuna tıklandıktan sonra açılan pencerede “Lenght” değeri 60 mm girilir ve yapılan 2 boyutlu çizime 60mm lik hacim kazandırılır.

 [![Şekil 6][11]][12]

[11]:  Ders3/6_res.png
[12]:  Ders3/6.png "Şekil 6"

Hacim kazandırma işlemi tamamlandıktan sonra oluşturulan gövdenin yan yüzeyi seçilir ve açılan pencerede kırmızı elips ile gösterilen “Create Sketch” butonuna tıklanarak yüzeyde çizim yapma işlemine başlanır.

 [![Şekil 7][13]][14]

[13]:  Ders3/7_res.png
[14]:  Ders3/7.png "Şekil 7"

Açılan çizim penceresinde kesimin yapılacağı şekil kırmızı çember içerisinde gösterilen “Create a line in the Sketch” komutu kullanılarak çizilir ve siyah dörtgen içerisinde gösterilen dikey ve yatay ölçülendirme komutları kullanırak ölçülendirmesi yapılır.

 [![Şekil 8][15]][16]

[15]:  Ders3/8_res.png
[16]:  Ders3/8.png "Şekil 8"

Kesim işlemi için gereken çizim tamamlandıktan sonra açılan pencerede, resimde kırmızı dörtgen içerisinde gösterilen “Pocket” komutuna tıklanır ve kesim işlemine başlanır.

 [![Şekil 9][17]][18]

[17]:  Ders3/9_res.png
[18]:  Ders3/9.png "Şekil 9"

“Pocket” butonuna tıklandıktan sonra açılan pencerede “Lenght” değeri 40 mm girilerek kesim işlemi tamamlanır.

 [![Şekil 10][19]][20]

[19]:  Ders3/10_res.png
[20]:  Ders3/10.png "Şekil 10"

Gövdenin resimde belirtilen yüzeyine tıklanır. Açılan pencerede resimde kırmızı elips içerisinde gösterilen “Create Sketch” komutuna tıklanır ve seçilen yüzey üzerinde çizim yapma işlemine başlanır.

 [![Şekil 11][21]][22]

[21]:  Ders3/11_res.png
[22]:  Ders3/11.png "Şekil 11"

Açılan çizim penceresinde kırmızı çember ile gösterilen çizgi komutu kullanılarak kesim yapılacak dörtgen çizilir ve siyah dörtgen içerisinde gösterilen dikey ve yatay ölçülendirme komutları kullanılarak dörtgenin ölçülendirmeleri yapılır.

 [![Şekil 12][23]][24]

[23]:  Ders3/12_res.png
[24]:  Ders3/12.png "Şekil 12"

Çizim tamamlandıktan sonra çıkan pencerede, resimde kırmızı dörtgen ile gösterilen “Pocket” butonuna tıklanarak delik açma işlemine başlanır.

 [![Şekil 13][25]][26]

[25]:  Ders3/13_res.png
[26]:  Ders3/13.png "Şekil 13"

“Pocket” komutuna tıklandığında açılan pencerede “Lenght” değeri 18mm olarak girilerek cep açma işlemi tamamlanır.

 [![Şekil 14][27]][28]

[27]:  Ders3/14_res.png
[28]:  Ders3/14.png "Şekil 14"

Cep açma işlemi tamamlandıktan sonra, resimde görülen yüzey seçilerek yüzey üzerinde çizim yapma işlemine başlanır.

[![Şekil 15][27]][28]

[27]:  Ders3/15_res.png
[28]:  Ders3/15.png "Şekil 15"

Yüzeydeki deliğin açılma işlemi için yapılacak olan çizimde, resimde kırmızı çember ile gösterilen “Create a Circle in the Sketch” komutu kullanılarak çember çizilir ve siyah çember ile gösterilen çember ölçülendirme komutu ile çizilen çemberin ölçülendirmesi yapılır.

[![Şekil 16][27]][28]

[27]:  Ders3/16_res.png
[28]:  Ders3/16.png "Şekil 16"

Çember çizimi tamamlandıktan sonra, resimde kırmızı elips ile gösterilen “Pocket” komutuna tıklanarak delik delme işlemine başlanır.

[![Şekil 17][27]][28]

[27]:  Ders3/17_res.png
[28]:  Ders3/17.png "Şekil 17"

“Pocket” komutuna tıklandıktan sonra “Lenght” değeri 10 mm girilerek delik açma işlemi tamamlanır.
