# Ders 1

 [![Teknik Resim][t1]][t2]

[t1]:  Ders1/teknik_resim_res.png
[t2]:  Ders1/teknik_resim.png "Teknik Resim"

Tasarıma başlamak için, öncelikle kırmızı yuvarlak ile gösterilen sekmelerden "Part Design" sekmesi seçilir.
Daha sonra kırmızı yuvarlak ile gösterilen "Create a New Sketch" sekmesi seçilerek, belirtilen parçanın 2 boyutlu çizimine başlanır. 

 [![Şekil 1][1]][2]

[1]:  Ders1/1_res.png
[2]:  Ders1/1.png "Şekil 1"


"Create a New Sketch" komutuna tıklandıktan sonra karşıya çıkan ekranda, çizime başlanılacak olan düzlem seçilir. (Örnek çizimde X-Y düzlemi seçilmiştir.)

 [![Şekil 2][3]][4]

[3]:  Ders1/2_res.png
[4]:  Ders1/2.png "Şekil 2"

Düzlem seçiminden sonra açılan çizim uzayında, kırmızı daire ile gösterilen "Create a Line in the Sketch" komutuna tıklanılarak, çizim yapmaya başlanır. Bu komut ile çizilen yatay çizgiler, siyah daire ile gösterilen "Fix the Horizontal Distance Between Two Point or Line Ends" komutu ile ölçülendirilir. Çizilen dikey çizgiler ise, yatay ölçülendirme komutunun yan kısmında bulunan ve mavi daire ile gösterilen "Fix the Vertical Distance Between Two Point or Line Ends" komutu ile ölçülendirilir.

 [![Şekil 3][5]][6]

[5]:  Ders1/3_res.png
[6]:  Ders1/3.png "Şekil 3"

Tasarımda bulunan delik kısmı için, kırmızı kare ile gösterilen "Create a Circle in the Sketcher" komutu ile bir çember çizilir. Çizilen bu çember, resimde kırmızı çember içerisinde gösterilmiş olan "Constrain an Arc or a Circle" komutu ile ölçülendirilir. Çemberin çizimi ve ölçülendirmesi tamamlandıktan sonra çember, siyah elips içinde gösterilen dikey ve yatay ölçülendirme komutları kullanılarak çizimde olması gereken  yere taşınır.

 [![Şekil 4][7]][8]

[7]:  Ders1/4_res.png
[8]:  Ders1/4.png "Şekil 4"

"Close" butonu ile 2 boyutlu çizim tamamlandıktan sonra, kırmızı çember ile gösterilen "Tasks" butonuna tıklanır. Açılan pencerede siyah çember ile gösterilen "Pad" komutuna tıklanılarak çizilen 2 boyutlu çizime hacim kazandırılma işlemine başlanır.

 [![Şekil 5][9]][10]

[9]:  Ders1/5_res.png
[10]:  Ders1/5.png "Şekil 5"

"Pad" komutuna  tıklandıktan sonra açılan pencerede, "Length" sekmesine 42 mm değeri girilerek, çizilmiş olan 2 boyutlu çizime belirtilen yönde 42 mm lik bir hacim kazandırılarak, çizim 3 boyutlu hale getirilir.

 [![Şekil 6][11]][12]

[11]:  Ders1/6_res.png
[12]:  Ders1/6.png "Şekil 6"

Tasarım 3 boyutlu hale getirildikten sonra ortaya çıkan gövdenin yan yüzeyine tıklanır ve çıkan pencerede, resimde kırmızı daire ile gösterilen "Create a New Sketch" komutuna tıklanılarak, seçilen yüzeyde çizim yapmaya başlanır.

 [![Şekil 7][13]][14]

[13]:  Ders1/7_res.png
[14]:  Ders1/7.png "Şekil 7"

Açılan çizim ekranında, siyah daire ile gösterilen çizgi komutu ile çizgiler çizilir ve kırmızı dörtgen içinde gösterilen ölçülendirme komutları kullanılarak çizilen dörtgenin ölçüleri ve pozisyonu ayarlanır.

 [![Şekil 8][15]][16]

[15]:  Ders1/8_res.png
[16]:  Ders1/8.png "Şekil 8"

Dörtgenin çizimi tamamlandıktan sonra "Close" butonuna tıklanarak çizim ekranından çıkılır. Çizim ekranından çıkıldıktan sonra açılan pencerede resimde kırmızı elips ile gösterilen "Pocket" komutuna tıklanarak, çizilmiş dörtgen kullanılarak kesim yapma işlemine geçilir.

 [![Şekil 9][17]][18]

[17]:  Ders1/9_res.png
[18]:  Ders1/9.png "Şekil 9"

"Pocket" komutuna tıklandıktan sonra açılan pencerede "Length" değeri 40 mm girilerek, çizilmiş olan dörtgen şeklinde 40 mm lik bir kesme işlemi uygulanır.

 [![Şekil 10][19]][20]

[19]:  Ders1/10_res.png
[20]:  Ders1/10.png "Şekil 10"

Kesim işlemi tamamlandıktan sonra, gövdenin ön yüzeyine tıklandığında çıkan pencerede kırmızı elips ile gösterilen "Create Sketch"  komutuna tıklanarak, ön yüzeyde çizim yapmaya başlanır.

 [![Şekil 11][21]][22]

[21]:  Ders1/11_res.png
[22]:  Ders1/11.png "Şekil 11"

Kırmızı daire ile gösterilen çizgi komutu kullanılarak bir dörtgen çizilir. Dörtgen çizimi bittikten sonra, siyah dörtgen içinde gösterilen dikey ve yatay ölçülendirme komutları kullanılarak, çizilen dörtgenin ölçüleri ve pozisyonu tanımlanır. "Close" butonuna basılarak çizim ekranından çıkılır.

 [![Şekil 12][23]][24]

[23]:  Ders1/12_res.png
[24]:  Ders1/12.png "Şekil 12"

Çizim ekranından çıkıldıktan sonra açılan pencerede, kırmızı dörtgen ile gösterilen "Pocket" butonuna tıklanılır ve  çizilen dörtgen kullanılarak kesim yapma işlemine geçilir.

 [![Şekil 13][25]][26]

[25]:  Ders1/13_res.png
[26]:  Ders1/13.png "Şekil 13"

"Pocket" butonuna tıklandıktan sonra çıkan pencerede "Length" değeri 10mm girilerek kesme işlemi tamamlanır.

 [![Şekil 14][27]][28]

[27]:  Ders1/14_res.png
[28]:  Ders1/14.png "Şekil 14"
