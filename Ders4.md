# Ders 4

 [![Teknik Resim][t1]][t2]

[t1]:  Ders4/teknik_resim_1.png
[t2]:  Ders4/teknik_resim_1.png "Teknik Resim"

 [![Teknik Resim][t3]][t4]

[t3]:  Ders4/teknik_resim_2.png
[t4]:  Ders4/teknik_resim_2.png "Teknik Resim"

Montaja başlamak için, resimde kırmızı elips ile gösterilen sekmede A2plus modülü seçilir ve sonrasında siyah dörtgen içerisinde gösterilen modül kullanılarak montajı yapılacak olan parçalar montaj uzayına eklenir.

 [![Şekil 1][1]][2]

[1]:  Ders4/1_res.png
[2]:  Ders4/1.png "Şekil 1"


Montaj esnasında kolaylık sağlaması için parçalar eklendikten sonra, kırmızı dörtgen içerisinde gösterilen komut kullanılarak montaj parçalarının pozisyonu düzeltilir.

 [![Şekil 2][3]][4]

[3]:  Ders4/2_res.png
[4]:  Ders4/2.png "Şekil 2"

Montajın ilk aşamasında, resimde yeşil renkle gösterilmiş deliklerin kenarları seçilerek, kırmızı daire içerisinde gösterilen komut kullanılarak, delik ve milin eş merkezli montajı yapılır.

 [![Şekil 3][5]][6]

[5]:  Ders4/3_res.png
[6]:  Ders4/3.png "Şekil 3"

Delik ve mil montajı tamamlandıktan sonra, ara parça montajı için, resimde yeşil renk ile gösterilen kenarlar seçilir ve kırmızı daire içerisinde gösterilen komut yardımıyla seçilen bölgelerin dairesel montajları yapılır.

 [![Şekil 4][7]][8]

[7]:  Ders4/4_res.png
[8]:  Ders4/4.png "Şekil 4"

Dairesel montaj butonuna tıklandığında karşıya çıkan pencerede, resimde kırmızı elips ile gösterilen “Offset” değeri değiştirilerek, hareketli ara parçanın pozisyonu ayarlanır.

 [![Şekil 5][9]][10]

[9]:  Ders4/5_res.png
[10]: Ders4/5.png "Şekil 5"

Hareketli ara parça ile uç menteşenin montajı için, hareketli ara parçada ve uç menteşede resimde yeşil renk ile gösterilen kenarlar seçilir, ardından resimde kırmızı çember içerisinde gösterilen dairesel montaj komutuna tıklanır.

 [![Şekil 6][11]][12]

[11]:  Ders4/6_res.png
[12]:  Ders4/6.png "Şekil 6"

Dairesel montaj komutuna tıklandığında, resimde kırmızı elips ile gösterilen “Offset” değeri değiştirilerek montajın pozisyonu ayarlanır.

 [![Şekil 7][13]][14]

[13]:  Ders4/7_res.png
[14]:  Ders4/7.png "Şekil 7"

Son aşamada, uç menteşe ve hareketli ara parça arasındaki bağlantıyı sağlayan milin montajı için, resimde yeşil renk ile gösterilen kenarlar seçilir ve ardından resimde kırmızı daire içerisinde gösterilmiş eş merkezli montaj komutu kullanılarak montaj tamamlanır.

 [![Şekil 8][15]][16]

[15]:  Ders4/8_res.png
[16]:  Ders4/8.png "Şekil 8"



