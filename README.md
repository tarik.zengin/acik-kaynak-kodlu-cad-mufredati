# Açık Kaynak Kodlu CAD Müfredatı Çalışması

Bu repoda, açık kaynak kodlu [FreeCAD](https://www.freecadweb.org/) programı kullanılarak, üniversiteler ve teknik eğitim kurumları için bilgisayar destekli tasarım (CAD) derslerinde kullanılmak üzere içerik üretilmesi planlanmıştır. Katkıda bulunmak isteyenler iletişime geçebilir.

Geliştiriciler: Fahri Anıl Selçuk, Aydın Tarık Zengin

## Dersler

[Ders 1](Ders1.md)

 [![Teknik Resim][t1]][t2]

[t1]:  Ders1/teknik_resim_res.png
[t2]:  Ders1.md "Ders 1"

[Ders 2](Ders2.md)

 [![Teknik Resim][t3]][t4]

[t3]:  Ders2/teknik_resim_res.png
[t4]:  Ders2.md "Ders 2"

[Ders 3](Ders3.md)

 [![Teknik Resim][t5]][t6]

[t5]:  Ders3/teknik_resim_res.png
[t6]:  Ders3.md "Ders 3"

[Ders 4](Ders4.md)

 [![Teknik Resim][t7]][t8]

[t7]:  Ders4/AssemblyDim.png
[t8]:  Ders4.md "Ders 4"